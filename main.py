from wordcloud import WordCloud, STOPWORDS 
import matplotlib.pyplot as plot
import re
from removeStopWords import clean

data = clean()
wc = WordCloud(width = 1920, height = 1080, background_color = "white", stopwords = STOPWORDS, min_font_size = 10).generate(data)

plot.figure(figsize = (8, 8), facecolor = None)
plot.imshow(wc)
plot.axis("off")
plot.tight_layout(pad = 0)
plot.show()
