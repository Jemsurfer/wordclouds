import re

def clean()->str:
    with open("input_english.txt", "r") as f:
      lines = f.readlines()
      content = " ".join(lines)

    with open("stopwords_english.txt", "r") as f:
      stopword_lines = f.readlines()
      stopwords = " ".join(stopword_lines)
      print(stopwords)

    splitted = re.split(r"\W",content)

    cleaned = []
    for i in splitted:
      if (not re.match(r"[a-zA-Z]+",i)) or (i.lower() in stopwords):
        pass #Remove punctuation & stopwords
      else:
        cleaned.append(i)
    
    cleaned_joined = " ".join(cleaned)
    return cleaned_joined
