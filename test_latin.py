from wordcloud import WordCloud
import matplotlib.pyplot as plot
import re

with open("lorem.txt", "r") as f:
  lines = f.readlines()
  content = " ".join(lines)
  
with open("stopwords_latin.txt", "r") as f:
  stopwords = f.readlines()
  stopwords_joined = " ".join(lines)

'''
with open("feynmann_lectures.txt", "r") as f:
  lines = f.readlines()
  content = " ".join(lines)

with open("stopwords_english.txt", "r") as f:
  stopwords = f.readlines()
  stopwords_joined = " ".join(lines)
  print(stopwords_joined)
'''

splitted = re.split("\W",content)
cleaned = []
for i in splitted:
  if (not re.match(r"[a-zA-Z]+",i)) or (i in stopwords):
    pass #Remove punctuation & stopwords
  else:
    cleaned.append(i)
    
cleaned_joined = " ".join(cleaned)
wc = WordCloud(width = 800, height = 800, background_color = "white", stopwords = stopwords_joined, min_font_size = 10).generate(cleaned_joined)
plot.figure(figsize = (8, 8), facecolor = None)
plot.imshow(wc)
plot.axis("off")
plot.tight_layout(pad = 0)
plot.show()
