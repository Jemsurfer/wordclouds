from collections import Counter
from removeStopWords import clean
import pandas as pd
import matplotlib.pyplot as plt

cnt = Counter()
content = clean() 

for c in content.split():
  cnt[c] += 1

word_freq = pd.DataFrame(cnt.most_common(20), columns=["words", "count"])

(fig, ax) = plt.subplots(figsize=(12, 8))

word_freq.sort_values(by='count').plot.barh(x="words",y="count", ax=ax, color="brown")
ax.set_title("Common words found in macbeth")
plt.show()
